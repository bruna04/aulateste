/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Aluno
 */
public class VendasTest {
    
    public VendasTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    /**
     * Test of addProduto method, of class Vendas.
     */
    @Test
    public void testAddProdutoCD() {
        Produto produto = new CD(10,"Parada","Luiz Gonzaga", 8, 3, 10.5); 
        Vendas venda = new Vendas();
        assertTrue(venda.addProduto(produto, 3));
       
    }
    @Test
    public void testAddProdutoLivro(){
        Produto prod = new Livros("sadasda", "teste", "Arnaldo", 5, 3, 15.3);
        Vendas vend = new Vendas();
        assertTrue(vend.addProduto(prod, 10));
    }

    /**
     * Test of valorTotal method, of class Vendas.
     */
    @Test
    public void testValorTotal() {
        Produto prod = new Livros("sadasda", "teste", "Arnaldo", 5, 3, 15.3);
        Vendas vend = new Vendas();
        vend.addProduto(prod, 10);
        assertEquals(153.0, vend.valorTotal(),0.00);
       
    }

    /**
     * Test of aplicarDesconto method, of class Vendas.
     */
    @Test
    public void testAplicarDesconto() {
       
    }
    
}
