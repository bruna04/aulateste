/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Aluno
 */
public class Livros extends Produto{
    private String isbn;

    public Livros(String isbn, String descricao, String nomeAutor, int quantidadeEstoque, int codigo, double valor) {
        super(descricao, nomeAutor, quantidadeEstoque, codigo, valor);
        this.isbn = isbn;
    }

    /**
     * @return the isbn
     */
    public String getIsbn() {
        return isbn;
    }

    /**
     * @param isbn the isbn to set
     */
    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    
}
