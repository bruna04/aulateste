/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Aluno
 */
public class CD extends Produto{
    private int nemuroFaixa;

    public CD(int nemuroFaixa, String descricao, String nomeAutor, int quantidadeEstoque, int codigo, double valor) {
        super(descricao, nomeAutor, quantidadeEstoque, codigo, valor);
        this.nemuroFaixa = nemuroFaixa;
    }

    

    

    /**
     * @return the nemuroFaixa
     */
    public int getNemuroFaixa() {
        return nemuroFaixa;
    }

    /**
     * @param nemuroFaixa the nemuroFaixa to set
     */
    public void setNemuroFaixa(int nemuroFaixa) {
        this.nemuroFaixa = nemuroFaixa;
    }
    
}
