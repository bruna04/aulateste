/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;

/**
 *
 * @author Aluno
 */
public class Vendas {

    private int codigo;
    private double desconto;
    private double valorTotal;

    ArrayList<ItemVendas> produto;

    public Vendas() {
        this.produto = new ArrayList<>();
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public double getDesconto() {
        return desconto;
    }

    public void setDesconto(double desconto) {
        this.desconto = desconto;
    }

    public double getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(double valorTotal) {
        this.valorTotal = valorTotal;
    }

    public ArrayList<ItemVendas> getProduto() {
        return produto;
    }

    public void setProduto(ArrayList<ItemVendas> produto) {
        this.produto = produto;
    }
    /**
     * addProduto é apra adicionar um produto
     * na lista de itens
     * @param produto
     * @param quantidade
     * @return 
     */

    public boolean addProduto(Produto produto, int quantidade) {
        boolean addProduto = true;

        for (int i = 0; i < this.produto.size(); i++) {
            if (this.produto.get(i).equals(produto)) {
                addProduto = false;
            }

        }

        if (addProduto == true) {
            ItemVendas item = new ItemVendas(quantidade, produto);
            this.produto.add(item);
        }
        return addProduto;

    }

    /**
     *valor total é para armazernar o valor total da compra
     * @return
     */

    public double valorTotal() {
        double valor = 0.0;

        for (int i = 0; i < this.produto.size(); i++) {
            valor += this.produto.get(i).getQuantidade() * this.produto.get(i).getProduto().getValor();
        }
        this.valorTotal = valor;

        return valorTotal;

    }

    /**
     * aplicarDesconto é para aplicar o desconto em cada cada valor total de compra
     *  
     * @return
     */
    public double aplicarDesconto() {

        double descontoTotal = 0;

        if (this.desconto > 0.0) {
            descontoTotal = this.desconto / 100.0;
            this.valorTotal -= this.valorTotal * descontoTotal;
        }

        return 0;

    }

}
