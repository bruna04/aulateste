/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.util.ArrayList;
import model.Vendas;

/**
 *
 * @author Aluno
 */
public class VendasDAO {
    private ArrayList<Vendas> venda;
    
    public VendasDAO() {
        this.venda = new ArrayList<>();
    }

    public ArrayList<Vendas> getVenda() {
        return venda;
    }

    public void setVenda(ArrayList<Vendas> venda) {
        this.venda = venda;
    }   
}
